# -*- coding: utf-8 -*- 
'''
test_blackjacktable.py
~~~~~~~~~~~~~~~~~~~~~~
This module tests the implementation of a blackjack table in python
'''

import unittest                   # unit testing framework
from blackjacktable import Card   # representation of a playing card
from blackjacktable import Shoe   # representation of a blackjack shoe
from blackjacktable import Hand   # representation of a blackjack hand 
from blackjacktable import Player # representation of a player at a blackjack table
from blackjacktable import Dealer # representation of a blackjack dealer, subclass of Player
from blackjacktable import Table  # representation of a blackjack table
from blackjacktable import Basic  # representation of a basic Player strategy to minimise loss over time

class blackjacktableTest(unittest.TestCase):
    
    def test1_Card001(self):
        ''' Card is an ace (Card.is_ace) '''
        card1 = Card(name='ace')
        card2 = Card(name='king')
        self.assertTrue(card1.is_ace())
        self.assertFalse(card2.is_ace())
        
    def test1_Card002(self):
        ''' Card is a cut card (Card.is_cut) '''
        card1 = Card.cut_card()
        card2 = Card(name='ace')
        self.assertTrue(card1.is_cut())
        self.assertFalse(card2.is_cut())
    
    def test1_Card003(self):
        ''' Equality between Cards (Card.__eq__) '''
        card1 = Card()
        card2 = card1
        card3 = Card(color='heart', name='ace')
        card4 = Card(color='heart', name='ace')
        card5 = Card(color='spade', name='ace')
        self.assertEqual(card1, card2)
        self.assertEqual(card3, card4)
        self.assertFalse(card3==card5)

    def test1_Card004(self):
        ''' Two cards have same value (Card.has_same_value) '''
        card1 = Card(name='king')
        card2 = Card(name='8')
        card3 = Card(name='queen')
        self.assertFalse(card1.has_same_value(card2))
    
    def test1_Card005(self):
        ''' Card is a face card (Card.is_face) '''
        card1 = Card(name='king')
        card2 = Card(name='jack')
        card3 = Card(name='queen')
        card4 = Card(name='8')
        self.assertTrue(card1.is_face())
        self.assertTrue(card2.is_face())
        self.assertTrue(card3.is_face())
        self.assertFalse(card4.is_face())
    
    
    def test2_Shoe001(self):
        ''' Draw card from shoe (Shoe.draw) '''
        shoe = Shoe(1) # shoe with one deck
        len_shoe = len(shoe)
        card = shoe.draw()
        self.assertTrue(isinstance(card, Card))
        self.assertFalse(card in shoe.cards)
        self.assertEqual(len_shoe, len(shoe)+1)
        
    def test2_Shoe002(self):
        ''' Cut card drawn from shoe (Shoe.cut_card_drawn) '''
        shoe = Shoe()
        self.assertTrue(Card.cut_card() in shoe.cards)
        card = shoe.draw()
        while not shoe.cut_card_drawn():
                card = shoe.draw()
        self.assertTrue(shoe.cut_card_drawn())
        
    def test2_Shoe003(self):
        ''' Trash Card(s) in the shoe (Shoe.trash) '''
        shoe = Shoe()
        card = shoe.draw()
        shoe.trash(card)
        self.assertTrue(len(shoe.trashs)==1)
        cards = [shoe.draw(), shoe.draw()]
        shoe.trash(cards)
        self.assertTrue(len(shoe.trashs)==3)
    
    def test2_Shoe004(self):
        ''' Shuffle a shoe (Shoe.shuffle) '''
        shoe = Shoe(1)
        shoe.trash(shoe.draw())
        shoe.shuffle()
        self.assertFalse(shoe.trashs)
        self.assertTrue(len(shoe.cards)==52+1)
    
    def test3_Hand001(self):
        ''' Add a card to the Hand (Hand.add) '''
        hand = Hand([])
        self.assertTrue(len(hand)==0)
        hand.add(Card())
        self.assertTrue(len(hand)==1)
        hand.add(Card())
        self.assertTrue(len(hand)==2)
        
    def test3_Hand002(self):
        ''' Hand contains an ace (Hand.has_ace) '''
        cards1 = [Card(name='2'), Card(name='10')]
        cards2 = [Card(name='queen'), Card(name='ace')]
        cards3 = [Card(name='ace'), Card(name='ace')]
        hand1 = Hand(cards1)
        hand2 = Hand(cards2)
        hand3 = Hand(cards3)
        self.assertFalse(hand1.has_ace())
        self.assertTrue(hand1.count_aces()==0)
        self.assertTrue(hand2.has_ace())
        self.assertTrue(hand2.count_aces()==1)        
        self.assertTrue(hand3.has_ace())
        self.assertTrue(hand3.count_aces()==2)        
        
    def test3_Hand003(self):
        ''' Values of a Hand (Hand.values) '''
        cards01 = [Card(name='8'), Card(name='10')]
        cards02 = [Card(name='3'), Card(name='8')]
        cards03 = [Card(name='queen'), Card(name='7')]
        cards04 = [Card(name='2'), Card(name='4'), Card(name='5')]
        cards05 = [Card(name='king'), Card(name='jack'), Card(name='ace')]
        cards06 = [Card(name='ace'), Card(name='6')]
        cards07 = [Card(name='ace'), Card(name='10')]
        cards08 = [Card(name='ace'), Card(name='ace')]
        cards09 = [Card(name='ace'), Card(name='ace'), Card(name='ace'), Card(name='2')]
        cards10 = [Card(name='ace'), Card(name='6'), Card(name='10'), Card(name='5')]
        hand01 = Hand(cards01)
        hand02 = Hand(cards02)
        hand03 = Hand(cards03)
        hand04 = Hand(cards04)
        hand05 = Hand(cards05)
        hand06 = Hand(cards06)
        hand07 = Hand(cards07)
        hand08 = Hand(cards08)
        hand09 = Hand(cards09)
        hand10 = Hand(cards10)
        self.assertEqual(hand01.value(), 18)
        self.assertEqual(hand02.value(), 11)
        self.assertEqual(hand03.value(), 17)
        self.assertEqual(hand04.value(), 11)
        self.assertEqual(hand05.value(), 21)
        self.assertEqual(hand06.value(), (7, 17))   
        self.assertEqual(hand07.value(), 21)
        self.assertEqual(hand08.value(), (2, 12))
        self.assertEqual(hand09.value(), (5, 15))
        self.assertEqual(hand10.value(), 22)
        
    def test3_Hand004(self):
        ''' Bets value of a Hand (Hand.best_value) '''
        cards1 = [Card(name='queen'), Card(name='7')]
        cards2 = [Card(name='ace'), Card(name='6')]
        cards3 = [Card(name='ace'), Card(name='10')]
        hand1 = Hand(cards1)
        hand2 = Hand(cards2)
        hand3 = Hand(cards3)       
        self.assertEqual(hand1.best_value(), 17)
        self.assertEqual(hand2.best_value(), 17)   
        self.assertEqual(hand3.best_value(), 21)        
       
    def test3_Hand005(self):
        ''' Hand is a natural blackjack (Hand.is_natural) '''
        cards1 = [Card(name='ace'), Card(name='10')]
        cards2 = [Card(name='ace'), Card(name='9'), Card(name='2')]
        cards3 = [Card(name='ace'), Card(name='6')]
        hand1 = Hand(cards1)
        hand2 = Hand(cards2)
        hand3 = Hand(cards3)       
        self.assertTrue(hand1.is_natural())
        self.assertFalse(hand2.is_natural())   
        self.assertFalse(hand3.is_natural())  

    def test3_Hand006(self):
        ''' Hand is over 21 (Hand.is_busted) ''' 
        cards1 = [Card(name='ace'), Card(name='10')]
        cards2 = [Card(name='2'), Card(name='4'), Card(name='5')]
        cards3 = [Card(name='ace'), Card(name='6'), Card(name='10'), Card(name='5')]
        hand1 = Hand(cards1)
        hand2 = Hand(cards2)
        hand3 = Hand(cards3)       
        self.assertFalse(hand1.is_busted())
        self.assertFalse(hand2.is_busted())   
        self.assertTrue(hand3.is_busted()) 

    def test3_Hand007(self):
        ''' Hand is a pair (Hand.is_pair) '''
        cards1 = [Card(name='ace'), Card(name='ace')]
        cards2 = [Card(name='2'), Card(name='2'), Card(name='4')]
        cards3 = [Card(name='king'), Card(name='6')]        
        hand1 = Hand(cards1)
        hand2 = Hand(cards2)
        hand3 = Hand(cards3)       
        self.assertTrue(hand1.is_pair())
        self.assertFalse(hand2.is_pair())   
        self.assertFalse(hand3.is_pair()) 

    def test3_Hand008(self):
        ''' Hand is hard hand, chance to bust on a hit (Hand.is_hard_hand) '''
        cards1 = [Card(name='ace'), Card(name='ace')]
        cards2 = [Card(name='2'), Card(name='2'), Card(name='4')]
        cards3 = [Card(name='king'), Card(name='6')]        
        hand1 = Hand(cards1)
        hand2 = Hand(cards2)
        hand3 = Hand(cards3)       
        self.assertFalse(hand1.is_hard_hand())
        self.assertFalse(hand2.is_hard_hand())   
        self.assertTrue(hand3.is_hard_hand())         
        
    def test3_Hand009(self):
        ''' Remove cards from the hand '''
        cards1 = [Card(name='ace'), Card(name='10')]
        hand1 = Hand(cards1)
        self.assertTrue(bool(hand1))
        hand1.clear()
        self.assertFalse(bool(hand1))
        self.assertEqual(hand1.value(), 0)
        
    def test3_Hand010(self): 
        ''' Hand is a combination of 2 faces (Hand.is_faces) '''
        cards1 = [Card(name='jack'), Card(name='10')]
        cards2 = [Card(name='jack'), Card(name='queen')]
        cards3 = [Card(name='king'), Card(name='queen')]
        cards4 = [Card(name='king'), Card(name='king')]
        self.assertFalse(Hand(cards=cards1).is_faces())
        self.assertTrue(Hand(cards=cards2).is_faces())
        self.assertTrue(Hand(cards=cards3).is_faces())
        self.assertTrue(Hand(cards=cards4).is_faces())
        
    def test3_Hand011(self):
        ''' Hand countains an ace counted as 11 (Hand.is_soft) '''
        cards1 = [Card(name='ace'), Card(name='8')]
        cards2 = [Card(name='ace'), Card(name='queen')]
        cards3 = [Card(name='ace'), Card(name='7'), Card(name='9')]
        self.assertTrue(Hand(cards=cards1).is_soft())
        self.assertFalse(Hand(cards=cards2).is_soft())
        self.assertFalse(Hand(cards=cards3).is_soft())
        
    def test3_Hand012(self):
        ''' Hand is soft 17, ace counted as 11 and Hand best value is 17 (Hand.is_soft_17) '''
        cards1 = [Card(name='ace'), Card(name='8')]
        cards2 = [Card(name='ace'), Card(name='6')]
        cards3 = [Card(name='ace'), Card(name='4'), Card(name='2')]
        self.assertFalse(Hand(cards=cards1).is_soft_17())
        self.assertTrue(Hand(cards=cards2).is_soft_17())
        self.assertTrue(Hand(cards=cards3).is_soft_17())        
    
    def test3_Hand013(self):
        ''' Double original bet on the hand (Hand.double_down) '''
        hand1 = Hand(bet=1)
        hand2 = Hand(bet=1)
        hand3 = Hand(bet=1)
        hand1.double_down()
        hand2.double_down(1)
        hand3.double_down(0.5)
        self.assertEqual(hand1.bet,2)
        self.assertEqual(hand2.bet,2)
        self.assertEqual(hand3.bet,1.5)
        
    
    def test4_Player001(self):
        ''' Clear hand(s) of the player (Player.clear) '''
        cards1 = [Card(name='2')]
        cards2 = [Card(name='queen'), Card(name='ace')]        
        cards3 = [Card(name='queen'), Card(name='7')]
        hands1 = [Hand(cards1)]
        hands2 = [Hand(cards1), Hand(cards2), Hand(cards3)]
        player0 = Player(hands=[])
        player1 = Player(hands=hands1)
        player2 = Player(hands=hands2)
        trashed0 = player0.clear()
        trashed1 = player1.clear()
        trashed2 = player2.clear()
        self.assertEqual(trashed0, [])
        self.assertEqual(player0.hands, [])
        self.assertEqual(trashed1, cards1)
        self.assertEqual(player1.hands, [])
        self.assertEqual(trashed2, cards1+cards2+cards3)
        self.assertEqual(player2.hands, [])

    def test4_Player002(self):
        ''' Hit a card on a Hand (Player.hit_hand) '''
        card1 = Card(name='queen')
        card2 = Card(name='2')
        player1 = Player(hands=[Hand(cards=[card1, card2])])
        shoe = Shoe()
        hand = player1.hands[0]
        player1.hit_hand(hand, shoe)
        self.assertEqual(len(hand.cards), 3)        
        
    def test4_Player003(self):
        ''' Double down a hand (Player.double_down_hand) '''
        card1 = Card(name='queen')
        card2 = Card(name='2')
        player1 = Player(hands=[Hand(cards=[card1, card2], bet=2)])
        shoe = Shoe()
        hand1 = player1.hands[0]       
        player1.double_down_hand(hand1, shoe, per=1)
        self.assertEqual(hand1.bet, 4)
        player2 = Player(hands=[Hand(cards=[card1, card2], bet=2)])
        hand2 = player2.hands[0] 
        player1.double_down_hand(hand2, shoe, per=0.5)
        self.assertEqual(hand2.bet, 3)
    
    def test4_Player004(self):
        ''' Split a hand (Player.split_hand) '''
        card1 = Card(name='queen')
        card2 = Card(name='queen')
        player1 = Player(hands=[Hand(cards=[card1, card2])])
        hand1 = player1.hands[0]
        shoe = Shoe()
        player1.split_hand(hand1, shoe)
        self.assertEqual(len(player1.hands), 2)
        self.assertEqual(len(player1.hands[0]), 2)
        self.assertEqual(len(player1.hands[1]), 2)
        
    def test4_Player005(self):
        ''' Surrender a hand (Player.surrender_hand) '''
        player = Player(deposit=10, hands=[Hand(cards=[Card(), Card()], bet=3)])
        hand = player.hands[0]
        player.surrender_hand(hand)
        self.assertTrue(hand.surrender)
        self.assertEqual(hand.bet, 1.5)
    
    def test4_Player006(self):
        ''' Initialise the Player's hand (Player.init_hand) '''
        player1 = Player(bet=3, seats=1)
        shoe = Shoe()
        player1.clear()
        player1.init_hand(shoe)
        self.assertTrue(len(player1.hands)==1)
        self.assertTrue(len(player1.hands[0])==1)
        self.assertTrue(player1.hands[0].bet==3)
        player2 = Player(bet=2, seats=4)
        player2.clear()
        player2.init_hand(shoe)
        self.assertTrue(len(player2.hands)==4)
        self.assertTrue(len(player2.hands[0])==1)
        self.assertTrue(len(player2.hands[3])==1)
    
    
    def test5_Dealer001(self):
        ''' Dealer to hit a Card (Dealer.hit)'''
        card1 = Card(name='queen')
        shoe = Shoe()
        dealer = Dealer(hands=[Hand([card1])])
        self.assertTrue(dealer.card)
        self.assertTrue(dealer.card==card1)
        self.assertTrue(len(dealer.hands)==1)
        self.assertTrue(len(dealer.hands[0])==1)
        self.assertTrue(dealer.hands[0].cards==[card1])
        dealer.hit(shoe)
        self.assertTrue(dealer.card)
        self.assertTrue(dealer.card == card1)
        self.assertTrue(len(dealer.hands)==1)
        self.assertTrue(len(dealer.hands[0])==2)
        
    def test5_Dealer002(self):
        ''' Clear the dealer hand (Dealer.clear) '''
        card1 = Card(name='queen')
        card2 = Card(name='2')        
        dealer = Dealer(hands=[Hand([card1, card2])])
        dealer.clear()
        self.assertFalse(dealer.hands)
        self.assertTrue(dealer.card is None)
        
    def test5_Dealer003(self):
        ''' Initialise hand of the dealer (Dealer.init_hand) '''
        dealer = Dealer()
        dealer.clear()
        self.assertFalse(dealer.hands)
        self.assertTrue(dealer.card is None)
        dealer.init_hand(Shoe())
        self.assertTrue(dealer.hands)
        self.assertFalse(dealer.card is None)
        self.assertTrue(len(dealer.hands)==1)
        
    def test5_Dealer004(self):
        ''' Dealer hit a card (Dealer.hit) '''
        card1 = Card(name='queen')
        card2 = Card(name='2')        
        shoe = Shoe()
        dealer = Dealer(hands=[Hand([card1, card2])])
        self.assertTrue(dealer.hands)
        self.assertTrue(len(dealer.hands)==1)
        self.assertTrue(len(dealer.hands[0])==2)
        dealer.hit(shoe)
        self.assertTrue(dealer.hands)
        self.assertTrue(len(dealer.hands)==1)
        self.assertTrue(len(dealer.hands[0])==3)        
        
    def test6_Table001(self):
        ''' Clear cards on the table (Table.clear) '''
        player = Player(seats=2, hands=[])
        dealer = Dealer(hands=[Hand([])])
        shoe = Shoe(number=8)
        table = Table(players=[player], dealer=dealer, shoe=shoe)
        table.deal()
        table.clear()
        self.assertTrue(table.shoe.trashs)
        self.assertTrue(len(table.shoe.trashs)==6)
        self.assertTrue(len(table.shoe.cards)==8*52+1-6)
        self.assertTrue(table.players[0].hands==[])
        self.assertTrue(table.dealer.hands==[])
    
    def test6_Table002(self):
        ''' Deal initials cards (Table.deal) '''
        shoe = Shoe().shuffle(random_seed=1)
        card1 = Card('heart', '5')
        card2 = Card('club', '8')
        card3 = Card('spade', 'queen')
        card4 = Card('club', '7')
        card5 = Card('heart', '9') 
        card6 = Card('spade', '7')
        player1 = Player(seats=1, hands=[])
        player2 = Player(seats=1, hands=[])
        table = Table(shoe=shoe, players=[player1, player2])
        table.deal()
        self.assertTrue(len(table.players[0].hands)==1)
        self.assertTrue(len(table.players[1].hands)==1)
        self.assertTrue(len(table.dealer.hands)==1)
        self.assertTrue(len(table.players[0].hands[0])==2)
        self.assertTrue(len(table.players[1].hands[0])==2)
        self.assertTrue(len(table.dealer.hands[0])==2) 
        self.assertTrue(table.players[0].hands[0].cards==[card1, card4])
        self.assertTrue(table.players[1].hands[0].cards==[card2, card5])
        self.assertTrue(table.dealer.hands[0].cards==[card3, card6])
    
    def test6_Table003(self):
        ''' Play one game (Table.play) '''
        shoe = Shoe().shuffle(random_seed=2)
        player1 = Player(seats=1, hands=[])
        cards1 = [Card('heart', '8'), Card('heart', '8'), Card('spade', '8')]
        cards2 = [Card('spade', 'queen'), Card('spade', '9')]
        cards3 = [Card('club', 'jack'), Card('heart', '4'), Card('diamond', 'ace'), Card('heart', '7')]
        player2 = Player(seats=1, hands=[])
        table = Table(shoe=shoe, players=[player1, player2])
        table.deal()
        table.play()
        self.assertTrue(len(table.players)==2)
        self.assertTrue(len(table.players[0].hands[0])==3)
        self.assertTrue(table.players[0].hands[0].cards==cards1)
        self.assertTrue(len(table.players[1].hands[0])==2)
        self.assertTrue(table.players[1].hands[0].cards==cards2)        
        self.assertTrue(len(table.dealer.hands[0])==4)
        self.assertTrue(table.dealer.hands[0].cards==cards3)

    def test6_Table004(self):
        ''' Collect bet amount (Table.collect) '''
        players = [Player(deposit=100, bet=2), Player(deposit=200, bet=3)]
        dealer = Dealer(deposit=0)
        table = Table(players=players, dealer=dealer)
        table.deal()
        table.collect()
        self.assertTrue(table.players[0].balance==100-2)
        self.assertTrue(table.players[1].balance==200-3)
        self.assertTrue(table.dealer.balance==5)
        
    def test6_Table005(self):
        ''' Pay player (Table.pay) '''
        players = [Player(deposit=100, bet=1)]
        dealer = Dealer(deposit=0)
        table = Table(players=players, dealer=dealer)
        cards1 = [Card(name='10'), Card(name='8')]
        cards2 = [Card(name='queen'), Card(name='ace')]
        table.players[0].hands = [Hand(cards=cards1, bet=2)]
        table.dealer.hands = [Hand(cards=cards2)]
        table.collect()
        table.pay()
        self.assertTrue(table.players[0].balance==98)
        self.assertTrue(table.dealer.balance==2)
        self.assertTrue(table.players[0].hands[0].bet==-2)

        table.players[0].hands = [Hand(cards=cards2, bet=2)]
        table.dealer.hands = [Hand(cards=cards2)]
        table.collect()
        table.pay()
        self.assertTrue(table.players[0].balance==98)
        self.assertTrue(table.dealer.balance==2)
        self.assertTrue(table.players[0].hands[0].bet==0)
        
        cards3 = [Card(name='10'), Card(name='8')]
        cards4 = [Card(name='queen'), Card(name='4'), Card(name='8')]
        table.players[0].hands = [Hand(cards=cards3, bet=5)]
        table.dealer.hands = [Hand(cards=cards4)]   
        table.collect()
        table.pay()        
        self.assertTrue(table.players[0].balance==103)
        self.assertTrue(table.dealer.balance==-3)  
        self.assertTrue(table.players[0].hands[0].bet==5)

        cards5 = [Card(name='10'), Card(name='8'), Card(name='5')]
        cards6 = [Card(name='queen'), Card(name='4'), Card(name='8')]
        table.players[0].hands = [Hand(cards=cards5, bet=4)]
        table.dealer.hands = [Hand(cards=cards6)]   
        table.collect()
        table.pay()        
        self.assertTrue(table.players[0].balance==99)
        self.assertTrue(table.dealer.balance==+1)
        self.assertTrue(table.players[0].hands[0].bet==-4)
        
        cards7 = [Card(name='10'), Card(name='7')]
        cards8 = [Card(name='10'), Card(name='8')]
        table.players[0].hands = [Hand(cards=cards7, bet=6)]
        table.dealer.hands = [Hand(cards=cards8)]   
        table.collect()
        table.pay()        
        self.assertTrue(table.players[0].balance==93)
        self.assertTrue(table.dealer.balance==+7)   
        self.assertTrue(table.players[0].hands[0].bet==-6)

        cards9 = [Card(name='10'), Card(name='9')]
        cards10 = [Card(name='10'), Card(name='8')]
        table.players[0].hands = [Hand(cards=cards9, bet=9)]
        table.dealer.hands = [Hand(cards=cards10)]   
        table.collect()
        table.pay()        
        self.assertTrue(table.players[0].balance==102)
        self.assertTrue(table.dealer.balance==-2) 
        self.assertTrue(table.players[0].hands[0].bet==9)  
        
        cards11 = [Card(name='10'), Card(name='queen')]
        cards12 = [Card(name='10'), Card(name='jack')]
        table.players[0].hands = [Hand(cards=cards11, bet=5)]
        table.dealer.hands = [Hand(cards=cards12)]   
        table.collect()
        table.pay()        
        self.assertTrue(table.players[0].balance==102)
        self.assertTrue(table.dealer.balance==-2)    
        self.assertTrue(table.players[0].hands[0].bet==0)
    
    def test6_Table006(self):
        ''' Shuffle the table shoe (Table.shuffle_shoe) '''
        table = Table()
        table.shoe.trash(table.shoe.draw())
        table.shoe.cut_card = True
        table.shuffle_shoe()
        self.assertFalse(table.shoe.trashs)
        
    def test7_Basic001(self):
        ''' Basic player plays a pair (Basic.play_pair) '''
        table = Table(split=1, double=True).round()
        basic = Basic()
        
        for name in Card.names:
            hand1=Hand([Card(name='ace'), Card(name='ace')])
            table.dealer.card = Card(name=name)
            basic.hands = [hand1]
            basic.play_pair(hand1, table)
            self.assertTrue(len(basic.hands)==2)
        
        for name in Card.names:
            hand2=Hand([Card(name='10'), Card(name='10')])
            hand3=Hand([Card(name='queen'), Card(name='queen')])        
            table.dealer.card = Card(name=name)
            basic.hands = [hand2]
            basic.play_pair(hand2, table)
            self.assertTrue(len(basic.hands)==1)
            self.assertTrue(len(basic.hands[0])==2)
            basic.hands = [hand3]
            basic.play_pair(hand3, table)
            self.assertTrue(len(basic.hands)==1)
            self.assertTrue(len(basic.hands[0])==2)
        
        
        for name in Card.names:
            hand4=Hand([Card(name='9'), Card(name='9')])
            table.dealer.card = Card(name=name)
            basic.hands = [hand4]
            basic.play_pair(hand4, table)
            if name in ['7','10','ace','jack','queen','king']:
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])==2)
            else:
                self.assertTrue(len(basic.hands)==2)

        for name in Card.names:
            hand5=Hand([Card(name='8'), Card(name='8')])
            table.dealer.card = Card(name=name)
            basic.hands = [hand5]
            basic.play_pair(hand5, table)  
            self.assertTrue(len(basic.hands)==2)
        
        for name in Card.names:
            hand6=Hand([Card(name='7'), Card(name='7')])
            table.dealer.card = Card(name=name)
            basic.hands = [hand6]
            basic.play_pair(hand6, table)            
            if name in [str(n) for n in range(2,8)]:
                self.assertTrue(len(basic.hands)==2)
            else:
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])>=3)

        for name in Card.names:
            hand7=Hand([Card(name='6'), Card(name='6')])
            table.dealer.card = Card(name=name)
            basic.hands = [hand7]
            basic.play_pair(hand7, table)            
            if name in [str(n) for n in range(2,7)]:
                self.assertTrue(len(basic.hands)==2)
            else:
                self.assertTrue(len(basic.hands)==1) 
                self.assertTrue(len(basic.hands[0])>=3)                
        
        
        for name in Card.names:
            hand8=Hand([Card(name='5'), Card(name='5')], bet=2)
            table.dealer.card = Card(name=name)
            basic.hands = [hand8]
            basic.play_pair(hand8, table)            
            if name in [str(n) for n in range(2,10)]:
                self.assertTrue(basic.hands[0].bet==4)
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])==3)
            else:
                self.assertTrue(len(basic.hands)==1) 
                self.assertTrue(len(basic.hands[0])>=3)
        
        for name in Card.names:
            hand9=Hand([Card(name='4'), Card(name='4')])
            table.dealer.card = Card(name=name)
            basic.hands = [hand9]
            basic.play_pair(hand9, table)   
            if name in ['5', '6']:
                self.assertTrue(len(basic.hands)==2)
            else:
                self.assertTrue(len(basic.hands)==1) 
                self.assertTrue(len(basic.hands[0])>=3)                

        for name in Card.names:
            for value in ['2', '3']:
                hand9=Hand([Card(name=value), Card(name=value)])
                table.dealer.card = Card(name=name)
                basic.hands = [hand9]
                basic.play_pair(hand9, table)   
                if name in [str(n) for n in range(2,8)]:
                    self.assertTrue(len(basic.hands)==2)
                else:
                    self.assertTrue(len(basic.hands)==1) 
                    self.assertTrue(len(basic.hands[0])>=3) 

    def test7_Basic002(self):
        ''' Basic player plays a soft hand (Basic.play_soft) '''
        table = Table(split=1, double=True).round()
        basic = Basic()
        
        for name in Card.names:
            hand8=Hand([Card(name='9'), Card(name='ace')])
            table.dealer.card = Card(name=name)
            basic.hands = [hand8]
            basic.play_soft(hand8, table)            
            self.assertTrue(len(basic.hands)==1)
            self.assertTrue(len(basic.hands[0])==2)       
    
        for name in Card.names:
            hand9=Hand([Card(name='ace'), Card(name='8')], bet=4)
            table.dealer.card = Card(name=name)
            basic.hands = [hand9]
            basic.play_soft(hand9, table)
            if not name=='6':                
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])==2)
            else:
                self.assertTrue(basic.hands[0].bet==8)
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])==3)
    
        for name in Card.names:
            hand10=Hand([Card(name='ace'), Card(name='7')], bet=1.5)
            table.dealer.card = Card(name=name)
            basic.hands = [hand10]
            basic.play_soft(hand10, table)
            if name in [str(n) for n in range(2,7)]:  
                self.assertTrue(basic.hands[0].bet==3)
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])==3) 
            elif name in ['7','8']:
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])==2)
            else:
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])>=3)
    
        for name in Card.names:
            hand10=Hand([Card(name='6'), Card(name='ace')], bet=2.5)
            table.dealer.card = Card(name=name)
            basic.hands = [hand10]
            basic.play_soft(hand10, table)
            if name in [str(n) for n in range(3,7)]:  
                self.assertTrue(basic.hands[0].bet==5)
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])==3) 
            else:
                self.assertTrue(basic.hands[0].bet==2.5)
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])>=3)       
        
        for name in Card.names:
            for value in ['4','5']:
                hand10=Hand([Card(name=value), Card(name='ace')], bet=3.5)
                table.dealer.card = Card(name=name)
                basic.hands = [hand10]
                basic.play_soft(hand10, table)
                if name in [str(n) for n in range(4,7)]:  
                    self.assertTrue(basic.hands[0].bet==7)
                    self.assertTrue(len(basic.hands)==1)
                    self.assertTrue(len(basic.hands[0])==3) 
                else:
                    self.assertTrue(basic.hands[0].bet==3.5)
                    self.assertTrue(len(basic.hands)==1)
                    self.assertTrue(len(basic.hands[0])>=3)         

        for name in Card.names:
            for value in ['2','3']:
                hand10=Hand([Card(name=value), Card(name='ace')], bet=4.5)
                table.dealer.card = Card(name=name)
                basic.hands = [hand10]
                basic.play_soft(hand10, table)
                if name in [str(n) for n in range(5,7)]:  
                    self.assertTrue(basic.hands[0].bet==9)
                    self.assertTrue(len(basic.hands)==1)
                    self.assertTrue(len(basic.hands[0])==3) 
                else:
                    self.assertTrue(basic.hands[0].bet==4.5)
                    self.assertTrue(len(basic.hands)==1)
                    self.assertTrue(len(basic.hands[0])>=3)   

                    
    def test7_Basic003(self):
        ''' Basic player play a hard hand (Basic.play_hard) '''
        table = Table(split=1, double=True).round()
        basic = Basic()

        for name in Card.names:
            for value in ['7', '8', '9', '10']:
                hand = Hand([Card(name='queen'), Card(name=value)])
                table.dealer.card = Card(name=name)
                basic.hands = [hand]
                basic.play_hard(hand, table)
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])==2)
        
        for name in Card.names:
            for value in [str(n) for n in range(2,7)]:
                hand = Hand([Card(name='jack'), Card(name=value)], bet=5)
                table.dealer.card = Card(name=name)
                basic.hands = [hand]
                basic.play_hard(hand, table)                    
                if name in [str(n) for n in range(2,7)] and not (value in ('2') and name in ('2','3')):
                    self.assertTrue(len(basic.hands)==1)
                    self.assertTrue(len(basic.hands[0])==2)  
                    self.assertTrue(basic.hands[0].bet==5)                     
                elif name in ['9','10','ace','jack','queen','king'] and value=='6':
                    self.assertTrue(len(basic.hands)==1)
                    self.assertTrue(len(basic.hands[0])==2) 
                    self.assertTrue(basic.hands[0].bet==2.5) 
                elif name in ['10','jack','queen','king'] and value=='5':
                    self.assertTrue(len(basic.hands)==1)
                    self.assertTrue(len(basic.hands[0])==2) 
                    self.assertTrue(basic.hands[0].bet==2.5) 
                else:
                    self.assertTrue(len(basic.hands)==1)
                    self.assertTrue(len(basic.hands[0])>=3)
                    self.assertTrue(basic.hands[0].bet==5)
        
        for name in Card.names:
            hand = Hand([Card(name='9'), Card(name='2')], bet=3)
            table.dealer.card = Card(name=name)
            basic.hands = [hand]
            basic.play_hard(hand, table)  
            self.assertTrue(len(basic.hands)==1)
            self.assertTrue(len(basic.hands[0])==3)
            self.assertTrue(basic.hands[0].bet==6)  
            
        for name in Card.names:
            hand = Hand([Card(name='7'), Card(name='3')], bet=10)
            table.dealer.card = Card(name=name)
            basic.hands = [hand]
            basic.play_hard(hand, table)
            if name in [str(n) for n in range(2,10)]:
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])==3)
                self.assertTrue(basic.hands[0].bet==20)  
            else: 
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])>=3)
                self.assertTrue(basic.hands[0].bet==10) 

        for name in Card.names:
            hand = Hand([Card(name='5'), Card(name='4')], bet=7.5)
            table.dealer.card = Card(name=name)
            basic.hands = [hand]
            basic.play_hard(hand, table)
            if name in [str(n) for n in range(3,7)]:
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])==3)
                self.assertTrue(basic.hands[0].bet==15)  
            else: 
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])>=3)
                self.assertTrue(basic.hands[0].bet==7.5)            
        
        for name in Card.names:
            for value in [str(n) for n in range(2,7)]:
                hand = Hand([Card(name='2'), Card(name=value)], bet=1)
                table.dealer.card = Card(name=name)
                basic.hands = [hand]
                basic.play_hard(hand, table)                
                self.assertTrue(len(basic.hands)==1)
                self.assertTrue(len(basic.hands[0])>=3)
        
if __name__ == '__main__': 
    unittest.main(exit=False, verbosity=2)