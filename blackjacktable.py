# -*- coding: utf-8 -*- 
'''
blackjacktable.py
~~~~~~~~~~~~~~~~~
This module implements a blackjack table in python. 

Main classes implemented in the module are 
    Card, a representation of a playing card for blackjack
    Shoe, a representation of a shoe of deck(s) of playing cards for blackjack
    Hand, a representation of a blackjack hand 
    Player, a representation of a player at a blackjack table 
    Skeleton, a skeleton Player class for an advance representation of a blackjack player 
    Dealer, a representation of a blackjack dealer
    Table, a representation of a blackjack table 

Main player classes implemented in the module are 
    NeverDraw, a representation of a simple player who never draws a card if hand can bust
    DrawOnce, a representation of a simple player who always draws one card if hand can bust
    Basic, a representation of player with blackjack basic strategy to minimise loss over time
    Learner, representation of a Player learning the best action on a blackjack table using reinforcement learning
'''

import random   # generate pseudo-random numbers
import numpy    # multi-dimensional arrays and matrices
import pandas   # data analysis tools

class Card(object):
    ''' Representation of a playing card for blackjack
    
    :color: string, color of the playing card (heart, diamond, spade, club). Can also be the cut card
    :name: string, name of the playing card (ace, 2 to 10, jack, quenn, king). Can also be the cut card
    :value: int or list of ints, value of the card in a blackjack game ([1,11] for ace, 5 for 5, 10 for jack)
    '''
    
    colors = ['heart', 'diamond', 'spade', 'club']
    names = ['ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king']
    values = {str(each): int(each) for each in range(2,11)}
    values.update({'jack': 10, 'queen': 10, 'king': 10, 'ace': (1, 11)})
    cut_cards = 'cut'
    
    def __init__(self, color=None, name=None):
        ''' Initialise a Card '''        
        if color is None:
            color = self.colors[random.randint(0, len(self.colors)-1)]
        if name is None:
            name = self.names[random.randint(0, len(self.names)-1)]
        self.color = color
        self.name = name
        self.value = self.values.get(self.name)
        return None

    @classmethod
    def cut_card(cls):
        ''' Initialise a cut card '''
        return cls(color=cls.cut_cards, name=cls.cut_cards)        
        
    def __str__(self):
        ''' How to print a Card '''
        return str(self.__class__.__name__)+str((self.color, self.name))
    
    __repr__ = __str__
    
    def __eq__(self, other):
        ''' Equality between two Cards '''
        return self.color==other.color and self.name==other.name
    
    def is_ace(self):
        '''  Whether a Card is an ace or not '''
        return self.name=='ace'
        
    def is_cut(self):
        ''' Whether a Card is a cut card '''
        return self.name==self.cut_cards
    
    def has_same_value(self, other):
        ''' Whether Card have same value as another '''
        return self.value==other.value
        
    def is_face(self):
        ''' Whether Card is a face card '''
        return self.name in ['jack', 'queen', 'king']
    

class Shoe(object):
    ''' Representation of a shoe of deck(s) of playing cards for blackjack
    
    :number: int, number of playing card decks in the shoe
    :cards: list of Cards, card queued in the shoe
    :trashs: list of Cards, card drawn or burned from the shoe 
    :cut_card: bool, whether the cut card has been drawn from the shoe
    '''
    
    deck = [Card(color, name) for name in Card.names for color in Card.colors]
    cut_card_range = [0.2, 0.8]
    
    def __init__(self, number=8):
        ''' Initialise and shuffle a Shoe '''
        self.number = number
        self.cards = []
        self.trashs = []
        self.cut_card = False
        self.shuffle()
        return None
        
    def __str__(self):
        ''' How to print a Shoe '''
        return str(self.__class__.__name__)+str((len(self.cards), len(self.trashs), self.cut_card))
    
    __repr__ = __str__ 
    
    def __len__(self):
        ''' Length of a shoe is number of Cards left to draw '''
        return len(self.cards)
    
    def draw(self):
        ''' Draw a Card from a Shoe '''
        card = self.cards.pop()
        if card.is_cut():
            self.cut_card = True
            card = self.cards.pop()
        return card

    def trash(self, cards):
        ''' Place Card(s) in trash '''
        try:
            self.trashs.extend(cards)
        except TypeError: 
            self.trashs.append(cards)
        return self
            
    def cut_card_drawn(self):
        ''' Whether the cut Card has been drawn '''
        return self.cut_card
    
    def insert_cut_card(self, range=cut_card_range):
        ''' Insert a cut card in a Shoe '''
        range = [int(each*len(self.cards)) for each in range]
        pos = random.randint(*range)
        self.cards.insert(pos, Card.cut_card())
        return self        
        
    def shuffle(self, random_seed=None):
        ''' Shuffle Cards in the Shoe '''
        if random_seed is not None:
            random.seed(random_seed)        
        self.cards = self.deck * self.number
        random.shuffle(self.cards)
        self.insert_cut_card()
        self.trashs = []
        self.cut_card = False
        return self


class Hand(object):
    ''' Representation of a blackjack hand 
    
    :bet: float, amount bet on the hand
    :cards: list of Cards, the cards in the hand
    :split: int, resplit number i.e. whether the hand is a split hand 
    :surrender: bool, whether the hand is surrendered
    '''
    
    def __init__(self, cards=None, bet=1, split=0, surrender=False):
        ''' Initialise a Hand '''
        if cards is None:
            cards = [Card(), Card()]
        self.bet = bet
        self.cards = cards  
        self.split = split
        self.surrender = surrender
        return None

    def __str__(self):
        ''' How to print a Hand '''
        return str(self.__class__.__name__)+str((self.bet, self.cards, self.value()))
        
    __repr__ = __str__
    
    def __len__(self):
        ''' Length of a Hand is the number of Cards in a Hand '''
        return len(self.cards)
        
    def __bool__(self):
        ''' Hand is empty if no Cards '''
        return not len(self)==0
    
    def double_down(self, per=1):
        ''' Increase initial bet by a percentage '''
        self.bet*=(1+per)
        return self
    
    def add(self, card=None):
        ''' Add a Card to a Hand '''
        if card is None:
            card = Card()
        self.cards.append(card)
        return self
        
    def count_aces(self):
        ''' Count the number of aces in a Hand '''
        return sum(card.is_ace() for card in self.cards)
    
    def has_ace(self):
        ''' Whether a Hand contains an ace '''
        return self.count_aces()>0
    
    def value(self):
        ''' Calculate value(s) of a Hand '''
        if not self.has_ace():
            value = sum(card.value for card in self.cards)
        else:
            non_ace = sum(card.value for card in self.cards if not card.is_ace())
            temp_value = [non_ace+self.count_aces(), 10+non_ace+self.count_aces()]
            value = [value for value in temp_value if value<=21]
            if len(value)==0:
                value = temp_value[0]
            elif value[-1]==21:
                value = 21
            elif len(value)==1:
                value = value[0]
            else:
                value=tuple(value)
        return value
    
    def best_value(self):
        ''' Best value of a Hand '''
        try:
            value = self.value()[-1]
        except TypeError:
            value = self.value()
        return value
            
    def is_soft(self):
        ''' Whether a Hand is a soft hand, contains an ace counted as 11 '''
        if self.has_ace():
            try:
                _ = self.value()[1]
                return True
            except TypeError:
                return False 
        return False
    
    def is_soft_17(self):
        ''' Whether a Hand contains an ace counted as 11 and best value is 17 '''
        return self.is_soft() and self.best_value()==17
            
    def is_21(self):
        ''' Whether a Hand is blackjack, natural or not '''
        return self.value()==21        
            
    def is_natural(self):
        ''' Whether a Hand is a natural blackjack '''
        return len(self)==2 and self.is_21()
    
    def is_busted(self):
        ''' Whether a Hand value is over 21 '''
        try:
            return self.value()[0]>21
        except TypeError: 
            return self.value()>21
            
    def is_pair(self):
        ''' Whether a Hand is a pair '''
        if len(self)==2:
            return self.cards[0].name==self.cards[1].name
        return False
        
    def is_faces(self):
        ''' Whether a Hand is a combination of 2 face Cards '''
        if len(self)==2:
            return self.cards[0].is_face() and self.cards[1].is_face()
        return False

    def is_hard_hand(self):
        ''' Whether there is a chance for a Hand to bust on a hit '''
        try: 
            return self.value()[0]>11
        except TypeError:
            return self.value()>11
            
    def clear(self):
        ''' Remove Cards from a Hand '''
        cards = list(self.cards)
        self.cards = []
        return cards


class Player(object):
    ''' Representation of a player at a blackjack table 
    
    :balance: float, amount a player can bet
    :deposit: float, initial balance or deposit of the player
    :bet: int, amount a player is betting per hand
    :seats: int, number of seats a player is sitting at the table
    :hands: list of Hands, hand(s) of the player
    '''
    
    def __init__(self, hands=None, deposit=500, bet=1, seats=1):
        ''' Initialise a player '''
        if hands is None:
            hands = [Hand(cards=[], bet=bet)]
        self.balance = deposit
        self.deposit = deposit
        self.bet = bet
        self.seats = seats
        self.hands = hands
        return None

    def __bool__(self):
        ''' Whether balance of a player is enough to bet '''
        return self.balance>=self.bet
        
    def __str__(self):
        ''' How to print a player '''
        return str(self.__class__.__name__)+str((self.balance, self.seats, self.hands, self.bet))
  
    __repr__ = __str__

    def clear(self):
        ''' Clear Hand(s) of a Player '''
        cards = []
        for hand in self.hands:
            cards.extend(hand.clear())
        self.hands = []
        return cards
        
    def init_hand(self, shoe):
        ''' Initialise the Player's hand(s) with a Card '''
        self.hands = [Hand(cards=[shoe.draw()], bet=self.bet) for seat in range(self.seats)]
        return self
    
    def is_sitting(self):
        ''' Whether the Player is sitting at the table '''
        return self.seats>0
     
    def hit_hand(self, hand, shoe):
        ''' Player takes another Card on a Hand '''
        hand.add(shoe.draw())
        return hand
    
    def stand_hand(self, hand, shoe):
        ''' Player stands on a hand '''
        pass
    
    def double_down_hand(self, hand, shoe, per=1):
        ''' Player increases initial bet by a percentage and hit one Card '''
        hand.double_down(per=per)
        self.hit_hand(hand, shoe)
        return hand
    
    def split_hand(self, hand, shoe):
        ''' Player splits a Hand into two Hands and add one Card to each '''
        hand1 = Hand(cards=[hand.cards[0], shoe.draw()], split=hand.split+1) 
        hand2 = Hand(cards=[hand.cards[1], shoe.draw()], split=hand.split+1)
        pos = self.hands.index(hand)
        self.hands[pos] = hand2
        self.hands[pos:pos] = [hand1]
        return [hand1, hand2]
        
    def surrender_hand(self, hand):
        ''' Player surrender hand and get back half of initial bet '''
        hand.surrender = True
        hand.bet-= 0.5*hand.bet
        return hand

    def play_hand(self, hand, table):
        ''' Player hand strategy based on a table for default player
        
        Pseudo random for the base Player: 
            on a hand <= 11, hit 
            on a hand <= 19, randomly decide to stand or hit 
            on a 20 or 21 hand, stand
        '''
        while not hand.is_busted():
            if hand.best_value()>19:
                break
            elif not hand.is_hard_hand():
                self.hit_hand(hand, table.shoe)
            elif random.randint(0, 1):
                self.hit_hand(hand, table.shoe)
            else:
                break
        return hand
        
    def play(self, table):
        ''' Player strategy based on a table '''
        for hand in self.hands:
            self.play_hand(hand, table)
        return self
    
    def update_bet(self, table):
        ''' Player betting strategy for default player, no change '''
        return self 
        
    def update_seats(self, table):
        ''' Player sitting strategy for default player, leave table if no balance or if double deposit '''
        if not self or self.balance>=2*self.deposit:
            self.seats = 0 
        return self

    def update(self, table):
        ''' Update Player betting size and sitting strategies '''
        return self.update_bet(table).update_seats(table)


class Skeleton(Player):
    ''' Skeleton Player class for an advance representation of a blackjack player 
    
    Minimum method requirements in a Player class is 
        play_hand: during a round table, decision on how to play a hand
        
    update_bet is run at the end of every round and can be used to collect information on the table
    update_seats is run at the end of every round and represent the player leaving the tabel or not
    '''
    
    def play_hand(self, hand, table):
        ''' Skeleton Player play strategy ''' 
        raise NotImplementedError

    def update_bet(self, table):
        ''' Skeleton Player betting strategy '''
        return Player.update_bet(self, table)
        
    def update_seats(self, table):
        ''' Skeleton Player sitting strategy '''
        return Player.update_seats(self, table)      

        
class Dealer(Skeleton):
    ''' Representation of a blackjack dealer, subclass of Player
    
    Inherit Skeleton and Player's attributes plus 
    :card: Card, exposed card of the dealer 
    ''' 
    
    def __init__(self, hands=None, deposit=0):
        ''' Initialise a dealer '''
        if hands is None:
            hands = [Hand()]
        Player.__init__(self, deposit=deposit, hands=hands)
        if self.hands[0]:
            self.card = self.hands[0].cards[0]
        else:
            self.card = None
        return None
        
    def __str__(self):
        ''' Overload, how to print a Dealer '''
        return str(self.__class__.__name__)+str((self.card, self.hands))
  
    __repr__ = __str__
    
    def clear(self):
        ''' Overload, also remove the exposed Card '''
        cards = Player.clear(self)
        self.card = None
        return cards
    
    def init_hand(self, shoe):
        ''' Overload, initialise the Dealer's hand(s) with a Card '''
        Player.init_hand(self, shoe)
        self.card = self.hands[0].cards[0]
        return self
    
    def hit(self, shoe):
        ''' Dealer to hit a card, hand never split '''
        Player.hit_hand(self, self.hands[0], shoe)
        return self

    def play_donot_hit_soft(self, table):
        ''' Dealer play behaviour, until soft 17 or busted '''
        hand = self.hands[0]
        while not (hand.is_busted() or hand.best_value()>=17):
            self.hit(table.shoe)
        return self
        
    
    def play_hit_soft(self, table):
        ''' Dealer play behaviour, until 17 or busted. Dealer to hit on a soft 17 '''
        hand = self.hands[0]
        while not (hand.is_busted() or (hand.best_value()>=17 and not hand.is_soft_17())):
            self.hit(table.shoe)
        return self

    def play(self, table, s17=False):
        ''' Dealer default strategy '''
        if s17:
            self.play_hit_soft(table)
        else: 
            self.play_donot_hit_soft(table)
        return self


class Table(object):
    ''' Representation of a blackjack table 
    
    :players: list of Players, players sitting at the table
    :dealer: Dealer, blackjack table dealer
    :shoe: Shoe, shoe of playing card decks 
    :natural_pay: float, payout for natural blackjack
    :s17: bool, whether the dealer hits on a soft 17 
    :surrender: bool, whether surrender is allowed
    :early_surrender: bool, if surrender: whether surrender is allowed against dealer natural blackjack
    :split: int, number of split and resplit allowed. None unlimitted resplit
    :double: bool, whether double down is allowed 
    :split_double: bool, if split and double: whether doubling down is allowed after a split
    '''
    
    def __init__(self, players=None, shoe=None, dealer=None
                , natural_pay=1.5, s17=True, surrender=True, early_surrender=True
                , split=None, double=True, split_double=True):
        ''' Initialise a Table '''
        if shoe is None:
            shoe = Shoe()
        if players is None:
            players = [Player(hands=[Hand([])])]
        if dealer is None:
            dealer = Dealer(hands=[Hand([])])
        self.dealer = dealer
        self.shoe = shoe
        self.players = players
        self.natural_pay = natural_pay
        self.s17 = s17
        self.surrender = surrender
        self.early_surrender = early_surrender
        self.split = split
        self.double = double
        self.split_double = split_double
        return None

    def __str__(self):
        ''' How to print a Table '''
        return str(self.__class__.__name__)+str((self.players, self.dealer, self.shoe))

    __repr__ = __str__  
    
    def __bool__(self):
        ''' Whether at least one player is sitting at a Table '''
        return any(player.is_sitting() for player in self.players)
    
    def shuffle_shoe(self):
        ''' Shuffle the Cards in the table Shoe if cut card drawn '''
        if self.shoe.cut_card_drawn():
            self.shoe.shuffle()
        return self
    
    def update_seats(self):
        ''' Check if Players are to stop sitting at a Table '''
        for player in self.players:
            player.update_seats(self)
        return self
    
    def update_bets(self):
        ''' Update Players bet amount '''
        for player in self.players:
            if player.is_sitting():
                player.update_bet(self)
        return self
    
    def deal(self):
        ''' Deal initial Cards '''
        for player in self.players+[self.dealer]:
            if player.is_sitting():
                player.init_hand(self.shoe)
        for player in self.players+[self.dealer]:
            if player.is_sitting():
                for hand in player.hands:
                    player.hit_hand(hand, self.shoe)        
        return self
        
    def play(self):
        ''' Players and Dealer play in turn '''
        for player in self.players:
            if player.is_sitting():
                player.play(self)
        self.dealer.play(self, s17=self.s17)
        return self
    
    def pay_player(self, player, payout):
        ''' Pay a Player from the Dealer balance '''
        player.balance+= payout
        self.dealer.balance-= payout
        return self
    
    def pay_dealer_busted(self):
        ''' Pay Players when Dealer hand is over 21 '''
        for player in self.players:
            if player.is_sitting():
                for hand in player.hands:
                    if not hand.is_busted() and not hand.surrender:
                        if hand.is_natural():
                            self.pay_player(player, (1.0+self.natural_pay)*hand.bet)
                            hand.bet = self.natural_pay*hand.bet
                        else:
                            self.pay_player(player, 2*hand.bet)
                    elif hand.is_busted() or hand.surrender:
                        hand.bet = -hand.bet
            else:
                for hand in player.hands:
                    hand.bet = 0 
        return self
    
    def pay_dealer_natural(self):
        ''' Pay Players when Dealer hand is a natural blackjack '''    
        for player in self.players:
            if player.is_sitting():
                for hand in player.hands:
                    if hand.is_natural() and not hand.surrender:
                        self.pay_player(player, hand.bet)
                        hand.bet = 0
                    else:
                        hand.bet=-hand.bet
            else:
                for hand in player.hands:
                    hand.bet = 0 
        return self
    
    def pay_simple(self):
        ''' Pay Players when Dealer hand is not over 21 and is not natural blackjack '''
        dealer_value = self.dealer.hands[0].best_value()
        for player in self.players:
            if player.is_sitting():
                for hand in player.hands:
                    if hand.is_natural() and not hand.surrender:
                        self.pay_player(player, (1.0+self.natural_pay)*hand.bet)
                        hand.bet = self.natural_pay*hand.bet
                    elif not hand.is_busted() and not hand.surrender:
                        hand_value = hand.best_value()
                        if hand_value>dealer_value:
                            self.pay_player(player, 2*hand.bet)
                        elif hand_value==dealer_value:
                            self.pay_player(player, hand.bet)
                            hand.bet = 0
                        else:
                            hand.bet = -hand.bet
                    else:
                        hand.bet = -hand.bet
            else:
                for hand in player.hands:
                    hand.bet = 0 
        return self
    
    def pay(self):
        ''' Pay Players '''
        if self.dealer.hands[0].is_busted():
            self.pay_dealer_busted()
        elif self.dealer.hands[0].is_natural():
            self.pay_dealer_natural()
        else:
            self.pay_simple()
        return self

    def collect(self):
        ''' Collect bets on the table '''
        for player in self.players:
            if player.is_sitting(): 
                for hand in player.hands:
                    self.pay_player(player, -hand.bet)
        return self
        
    def clear(self):
        ''' Clear Cards on the table '''
        card_drawn = []
        for player in self.players+[self.dealer]:
            card_cleared = list(player.clear())
            card_drawn.extend(card_cleared)
        self.shoe.trash(card_drawn)
        return self
        
    def round(self):
        ''' Round of blackjack '''
        return self.clear().deal().play().collect().pay().shuffle_shoe().update_bets().update_seats()
    
    def simulate(self):
        ''' Simulate a Table '''
        while self:
            self.round()
        return self


class NeverDraw(Skeleton):
    ''' Representation of a simple blackjack Player who never draws a card if hand can bust '''
    
    def play_hand(self, hand, table):
        ''' Draw a crad until hand can bust '''
        while not (hand.is_busted() or hand.is_hard_hand()):
            self.hit_hand(hand, table.shoe)
        return self


class DrawOnce(NeverDraw):
    ''' Representation of a simple blackjack Player who always draws one card if hand can bust '''
    
    def play_hand(self, hand, table):
        ''' Always draw one and only one card if hand can bust '''
        NeverDraw.play_hand(self, hand, table)
        self.hit_hand(hand, table.shoe)
        return self


class Basic(Skeleton):
    ''' Representation of a basic Player strategy to minimise loss over time
            https://en.wikipedia.org/wiki/Blackjack#Basic_strategy
    '''
    
    pair_split = [(9,n) for n in range(2,10) if not n==7]
    pair_split+= [(7,n) for n in range(2,8)]
    pair_split+= [(6,n) for n in range(2,7)]
    pair_split+= [(4,5), (4,6)]
    pair_split+= [(i,n) for i in range(2,4) for n in range(2,8)]
    pair_stand = [(9,7), (9,10), (9,(1,11))]
    pair_double_hit = [(5,n) for n in range(2,10)]
    soft_stand = [(7,7), (7,8)]
    soft_stand+= [(8,n) for n in range(2,11) if not n==6]+[(8,(1,11))]
    soft_double_stand = [(8,6), (7,2), (7,3), (7,4), (7,5), (7,6)]
    soft_double_hit = [(6,n) for n in range(3,7)]
    soft_double_hit+= [(i,n) for i in range(4,6) for n in range(4,7)]
    soft_double_hit+= [(i,n) for i in range(2,4) for n in range(5,7)]
    hard_surrender = [(16,9), (16,10), (16,(1,11)), (15,10)]
    hard_stand = [(i,n) for i in range(13,17) for n in range(2,7)]
    hard_stand+= [(12,n) for n in range(4,7)]
    hard_double_hit = [(10,n) for n in range(2,10)]
    hard_double_hit+= [(9,n) for n in range(3,7)]
    
    def play_hand(self, hand, table):
        ''' Basic Hand strategy '''
        if hand.best_value()>=21:
            return self
        elif hand.is_pair():
            self.play_pair(hand, table)
        elif hand.is_soft():
            self.play_soft(hand, table)
        else:
            self.play_hard(hand, table)
        return hand
            
    def play_pair(self, hand, table):
        ''' Strategy if hand is a pair '''
        state = (hand.cards[0].value, table.dealer.card.value)
        if  (hand.cards[0].is_ace() or hand.cards[0].value==8 or state in self.pair_split):
            if table.split is None or table.split > hand.split:
                [hand1, hand2] = self.split_hand(hand, table.shoe)
                self.play_hand(hand1, table)
                self.play_hand(hand2, table)
            else:
                self.play_hard(hand, table)
        elif (hand.cards[0].value==10 or state in self.pair_stand):
            self.stand_hand(hand, table.shoe)
        elif (table.double and state in self.pair_double_hit):
            self.double_down_hand(hand, table.shoe)
        else:
            self.hit_hand(hand, table.shoe)
            self.play_hand(hand, table)
        return self
    
    def play_soft(self, hand, table):
        ''' Strategy if initial hand has an ace '''
        not_ace = sum(card.value for card in hand.cards if not card.is_ace())
        state = (not_ace, table.dealer.card.value)
        if  (not_ace==9 or state in self.soft_stand):
            self.stand_hand(hand, table.shoe)
        elif state in self.soft_double_stand:
            if table.double and table.split_double:
                self.double_down_hand(hand, table.shoe)
            elif table.double and not table.split_double and not hand.split>0:
                self.double_down_hand(hand, table.shoe)
            else:
                self.stand_hand(hand, table.shoe)
        elif table.double and state in self.soft_double_hit:
            if table.split_double:
                self.double_down_hand(hand, table.shoe)
            elif not table.split_double and not hand.split>0:
                self.double_down_hand(hand, table.shoe)
            else:
                self.hit_hand(hand, table.shoe)
                self.play_hand(hand, table)
        else:
            self.hit_hand(hand, table.shoe)
            self.play_hand(hand, table)
        return self
        
        
    def play_hard(self, hand, table):
        ''' Strategy based on hand hard total '''
        state = (hand.best_value(), table.dealer.card.value)
        if  (hand.best_value() in (17,18,19,20) or state in self.hard_stand):
            self.stand_hand(hand, table.shoe)
        elif len(hand)==2 and state in self.hard_surrender:
            if table.surrender and table.early_surrender:
                self.surrender_hand(hand)
            elif table.surrender and not table.early_surrender and not table.dealer.hands[0].is_natural():
                self.surrender_hand(hand)
            else:
                self.hit_hand(hand, table.shoe)
                self.play_hand(hand, table)                
        elif (table.double and (hand.best_value()==11 or state in self.hard_double_hit)):
            if table.split_double:
                self.double_down_hand(hand, table.shoe)
            elif not table.split_double and not hand.split>0:
                self.double_down_hand(hand, table.shoe)
            else:
                self.hit_hand(hand, table.shoe)
                self.play_hand(hand, table)                
        else:
            self.hit_hand(hand, table.shoe)
            self.play_hand(hand, table)
        return self            


class Learner(Skeleton):
    ''' Representation of a Player learning the best action on a blackjack table using reinforcement learning
    
    On top of Skeleton's attribute 
    :action: dic, represent the history of events the Learner is learning from 
    :action_hand: dictionary state and actions, represent learner action on a hand
    :rate: int, represent the number of hands for exploration before exploitation
    '''
    
    actions = [
        Skeleton.hit_hand
        , Skeleton.stand_hand
        , Skeleton.surrender_hand
        , Skeleton.double_down_hand
        , Skeleton.split_hand
        ]
    columns = [2, 3, 4, 5, 6, 7, 8, 9, 10, (1, 11)]
    pair = [(2, 12), 20, 18, 16, 14, 12, 10, 8, 6, 4]
    soft = [(10, 20), (9, 19), (8, 18), (7, 17), (6, 16), (5, 15), (4, 14), (3, 13)] 
    hard = [20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5]
    
    def __init__(self, deposit=500, bet=1, seats=1, hands=None, rate=100):
        ''' Initialise a Learner '''
        if hands is None:
            hands = [Hand(cards=[], bet=bet)]
        Skeleton.__init__(self, deposit=deposit, bet=bet, seats=seats, hands=hands)
        self.action = {}
        self.action_hand = []
        self.rate = rate
        return None
    
    def random_action(self, hand, table):
        ''' Random action for exploration '''
        if hand.is_pair():
            action = self.actions[random.randint(0, len(self.actions)-1)]
        else:
            action = self.actions[random.randint(0, len(self.actions)-2)]
        return action
        
    def best_action(self, hand, table):
        ''' Best action for exploitation '''
        state = self.get_state(hand, table)
        actions = self.action.get(state, None)
        if actions is None:
            action = self.random_action(hand, table)
        else:
            action = max(actions, key=(lambda key: actions.get(key).get('value')))
        return action
        
    def greedy_action(self, epsilon, hand, table):
        ''' Epsilon greedy choice of action '''
        if random.random()<epsilon:
            action = self.random_action(hand, table)
        else:
            action = self.best_action(hand, table)
        return action
    
    def get_state(self, hand, table):
        ''' Calculate state for the hand and table '''
        return (hand.value(), table.dealer.card.value, hand.is_pair())
    
    def count_action(self, hand, table):
        ''' Count the total number of actions for a state '''
        state = self.get_state(hand, table) 
        actions = self.action.get(state, [])
        return sum(actions.get(action).get('count') for action in actions)
    
    def update_action_hand(self, hand, state, action):
        ''' Update learner actions on a hand '''
        self.action_hand.append((hand, state, action))
        return self
    
    def update_action(self, state, action, reward):
        ''' Update the Learner's actions ''' 
        if action is None:
            pass
        elif state not in self.action:
            self.action[state]={action: {'count': 1, 'value': reward}}
        elif action not in self.action[state]:
            self.action[state][action] = {'count': 1, 'value': reward}
        else:
            alpha = (1.0/self.action[state][action]['count'])
            beta = (reward-self.action[state][action]['value'])
            self.action[state][action]['count']+=1
            self.action[state][action]['value']+=alpha*beta
        return self
    
    def update_bet(self, table):
        ''' Update Learner action at the end of a round '''
        for hand, state, action in self.action_hand:
            self.update_action(state, action, hand.bet)
        self.action_hand = []
        return self
    
    def play_hand(self, hand, table):
        ''' Learner hand strategy '''
        if not (hand.is_busted() or hand.is_21()):
            epsilon = self.rate / (self.rate+self.count_action(hand, table))
            function = self.greedy_action(epsilon, hand, table)
            state = self.get_state(hand, table)
            if function==Skeleton.stand_hand:
                self.stand_hand(hand, table.shoe)
                self.update_action_hand(hand, state, Skeleton.stand_hand)
            elif function==Skeleton.hit_hand:
                self.hit_hand(hand, table.shoe)
                self.update_action_hand(hand, state, Skeleton.hit_hand)
                self.play_hand(hand, table)
            elif function==Skeleton.split_hand:
                [hand1, hand2] = self.split_hand(hand, table.shoe)
                self.play_hand(hand1, table)
                self.play_hand(hand2, table)
                self.update_action_hand(hand1, state, Skeleton.split_hand)
                self.update_action_hand(hand2, state, Skeleton.split_hand)
            elif function==Skeleton.surrender_hand:
                self.surrender_hand(hand)
                self.update_action_hand(hand, state, Skeleton.surrender_hand)
            elif function==Skeleton.double_down_hand:
                self.double_down_hand(hand, table.shoe)
                self.update_action_hand(hand, state, Skeleton.double_down_hand)
        return self

    def summary_best_actions(self):
        ''' Summary of Learner best actions '''
        dic = {True: {}, False: {}}
        for state in self.action:
            actions = self.action.get(state)
            action = max(actions, key=(lambda key: actions.get(key).get('value')))
            if state[0] not in dic[state[2]]:
                dic[state[2]][state[0]] = {state[1]: action.__name__}
            else:
                dic[state[2]][state[0]][state[1]] = action.__name__
        return dic
        
    def matrix_best_actions(self):
        ''' Convert dictionary of best actions to 3 matrices '''
        dic = self.summary_best_actions()
        mat_list = []
        for each in [self.hard, self.soft, self.pair]:
            mat = numpy.chararray((len(each), len(self.columns)), itemsize=30, unicode=True)
            for i in each:
                for j in self.columns:
                    ind_i, ind_j = each.index(i), self.columns.index(j)
                    if each == self.pair:
                        mat[ind_i, ind_j] = dic.get(True).get(i).get(j)
                    else: 
                        mat[ind_i, ind_j] = dic.get(False).get(i).get(j)
            mat_list.append(mat)
        return tuple(mat_list)
        
    def dataframe_best_actions(self):
        ''' Convert dictionary of best actins to 3 dataframes '''
        hard, soft, pair = self.matrix_best_actions()
        hard = pandas.DataFrame(hard, columns=self.columns, index=self.hard)
        soft = pandas.DataFrame(soft, columns=self.columns, index=self.soft)
        pair = pandas.DataFrame(pair, columns=self.columns, index=self.pair)
        return tuple([hard, soft, pair])