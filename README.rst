Blackjack implementation in Python
==================================

This repository contains an object-oriented implementation of a blackjack table in Python. 
The implementation includes    

1) a series of classes to represent each element of a blackjack table such as Card, Shoe, Hand, Player and Table   
2) a series of extensive tests for each of the classes using unittest framework  
3) an implementation of a basic strategy to minimise loss over time 
4) an implementation of a player learning the best strategy using reinforcement learning   


|

Blackjack in a nutshell 
-----------------------

Blackjack is a card game played between several players and a dealer, where each player in turn competes against the dealer. 
It is played with one or more decks of 52 playing cards stored in a shoe. The objective of the game is to beat the dealer

Players are dealt two cards face up and the dealer is also dealt two cards, with one up (exposed) and one down, which value is unknown for the players. 
The value of cards two through ten is their pip value (2 through 10). Face cards (Jack, Queen, and King) are all worth ten. Aces can be worth one or eleven. 
A hand's value is the sum of the card values. Players are allowed to draw additional cards to improve their hands. A hand with an ace valued as 11 is called "soft", 
meaning that the hand will not bust by taking an additional card; the value of the ace will become one to prevent the hand from exceeding 21. Otherwise, the hand is "hard"

Once all the players have completed their hands, it is the dealer's turn.
The dealer then reveals the hidden card and must hit until the cards total 17 or more points. 
Players win by not busting and having a total higher than the dealer, or not busting and having the dealer bust, or getting a blackjack without the dealer getting a blackjack. 
If the player and dealer have the same total (not counting blackjacks), this is called a "push" or a  "tie", and the player typically does not win or lose money on that hand. Otherwise, the dealer wins

|

Basic usage 
-----------

First object implemented is a representation of a playing card for blackjack. 
A **card** is defined by a color *color*, a name *name* and a value *value* in a blackjack game     

.. code-block:: python
    
    card1 = Card(color='heart', name='queen') # queen of heart card
    card2 = Card(name='ace')                  # ace of a random color
    print(card1, card2)
	
    print(Card.colors) # list all card colors 
    print(Card.names)  # list all card names

Second object is a representation of a shoe of deck(s) of playing cards for blackjack. 
A **shoe** is defined by a number of deck *number* in the shoe, 
a list of cards *cards* to be drawn, a list of cards trashed *trashs* 
and a boolean *cut_card* whether the cut card has been drawn 

.. code-block:: python
    
    shoe = Shoe(8)      # shoe with 8 decks
    print(shoe)
    card = shoe.draw()  # draw a card 
    shoe.trash(card)    # put the card in trash
    print(shoe)

Third object is a representation of a blackjack hand. 
A **Hand** is defined by a bet amount *bet*, a list of cards *cards*, 
a boolean *split* whether a hand is plit and a boolean *surrender* whether the hand is surrendered

.. code-block:: python
    
    cards = [Card(color='heart', name='queen'), Card(name='ace')]
    hand = Hand(bet=1, cards=cards) # a 1 value bet on a hand of queen and ace 
    print(hand)

Fourth object is a base representation of a player at a blackjack table. 
A base **Player** is defined by a an amount available to bet *balance*, an initial deposit *deposit*, 
an amount *bet* a player is betting per hand, a number of seats *seats* a player is sitting at the table and the list of hands of the player *hands* 

.. code-block:: python

    hand = Hand([Card(name='2'), Card(name='9')])
    player = Player(deposit=500, hands=[hand], bet=1, seats=1) # player with 500 deposit, bet 1 per hand and use one seat at the table
    print(player)

The representation of a blackjack dealer is a sub-class of Player. 
A **Dealer** inherits all attributes plus the exposed card of the dealer *card*

.. code-block:: python

    hand = Hand(cards=[Card(name='ace')])
    dealer = Dealer(hands=[hand])
    print(dealer)

The last object is a representation of a blackjack table. 
A **Table** is defined is defined by a list of players *players*, a dealer *dealer* and a *shoe*. 
A table also have other attributes to represent the various blackjack rules 
(natural blackjack payout, whether the dealer hits on a soft 17, whether surrender is allowed, whether split is allowed, whether multiple split is allowed etc)

.. code-block:: python

    player = Player()
    dealer = Dealer()
    shoe = Shoe(8)
    table = Table(players=[player], dealer=dealer, shoe=shoe)
    print(table)
    table.round()        # play a round at the table (bet, play, collect and bet)
    table.round()        # play another round
    print(table)
    table.simulate()     #  play until each players leaves the table 
|

Blackjack Player Behaviour  
--------------------------

A number of pre-defined blackjack strategies have been implemented via respective sub-classes of a skeleton player. 
The **Skeleton** player represent the minimum methods to overload to implement a blackjack strategy 

- *play_hand*: mandatory implementation. Represent how the player will play a hand given the table
- *update_bet*: optional implementation. Method is called at the end of a round after the dealer collect bets and pay wins. Represent how the player will change betting strategy (increase bet size or probability to hit a cretain hand). Default is no change in strategy 
- *update_seats*: optional implementation. Method is called at the end of a round after *update_bet*. Represent whether the player will exit the table or increase sittings. Default is leave table if player has no more balance or if player has double initial deposit

Examples of blackjack strategies implemented are 

- *NeverDraw*: a representation of a simple player who never draws a card if hand can bust (i.e. hand over 11)
- *DrawOnce*: a representation of a simple player who always draws one single card if hand can bust (i.e. hand over 11)
- *Basic*: a representation of a player with a basic strategy to minimise loss over time (`Wikipedia Basic Strategy`_)
- *Learner*: a representation of a player learning best strategy using reinforcement learning (`Wikipedia Reinforcement Learning`_)

.. _`Wikipedia Basic Strategy`: https://en.wikipedia.org/wiki/Blackjack#Basic_strategy
.. _`Wikipedia Reinforcement Learning`: https://en.wikipedia.org/wiki/Reinforcement_learning